package com.example.leo.sobekick;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GetStartedActivity extends AppCompatActivity {
 private Button mGetstart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);



        mGetstart =(Button)findViewById(R.id.getstarted_btn);
        VideoView videoView =(VideoView)findViewById(R.id.videoview);
        /*Window window = this.getWindow();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }*/
        //Creating MediaController
        //boolean vlog = getIntent().getBooleanExtra("log",false);
        //if(!vlog) {
            String path = "android.resource://" + getPackageName() + "/" + R.raw.intro;

            videoView.setVideoURI(Uri.parse(path));
            videoView.start();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.setLooping(true);
                }
            });


            mGetstart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startActivity(new Intent(GetStartedActivity.this, HomeActivity.class).putExtra("onDemand",false));
                    finish();
                }
            });
//        }else {
//            startActivity(new Intent(GetStartedActivity.this, HomeActivity.class));
//        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}
