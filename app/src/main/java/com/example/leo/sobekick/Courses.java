package com.example.leo.sobekick;

/**
 * Created by leo on 10/9/17.
 */

public class Courses {

    private String c_name,sub_cat,iframe_url;

    public Courses(String c_name, String sub_cat,String url) {
        this.c_name = c_name;
        this.sub_cat = sub_cat;
        this.iframe_url = url;

    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getIframe_url() {
        return iframe_url;
    }

    public void setIframe_url(String iframe_url) {
        this.iframe_url = iframe_url;
    }
/*    public String getInstuctor() {
        return instuctor;
    }

    public void setInstuctor(String instuctor) {
        this.instuctor = instuctor;
    }*/


    public String getSub_cat() {
        return sub_cat;
    }

    public void setSub_cat(String sub_cat) {
        this.sub_cat = sub_cat;
    }
}
