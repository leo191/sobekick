package com.example.leo.sobekick.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.leo.sobekick.Adapters.CourseAdapter;
import com.example.leo.sobekick.Adapters.FilterAdapter;
import com.example.leo.sobekick.Callbacks.GetCourses;
import com.example.leo.sobekick.Courses;
import com.example.leo.sobekick.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnDemandFragment extends Fragment implements View.OnClickListener {


    TextView filter;
    RelativeLayout fl;
    View v;
    LinearLayout filter_lay;
    LinearLayout blur;
    DrawerLayout drawerLayout;
    ListView filterList;
    ArrayAdapter<String> adapter;
    ArrayList<String> fils ;
    private RecyclerView recyclerView;
    private CourseAdapter courseAdapter;
    private ArrayList<Courses> sample;
    ArrayList<Courses> complete ;


    public OnDemandFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getContext());

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View main =  inflater.inflate(R.layout.fragment_on_demand, container, false);
        /*filter_lay = (LinearLayout)main.findViewById(R.id.filter_layout);
        filter_lay.setVisibility(View.GONE);
*/      sample =  new ArrayList<>();
        complete =new ArrayList<Courses>();
        filterList = (ListView)main.findViewById(R.id.right_draw_list);
        drawerLayout = (DrawerLayout)main.findViewById(R.id.drawer_layout);
        recyclerView = (RecyclerView)main.findViewById(R.id.recycler_view);
        //sample.add(new Courses("Boxing","Monday | some","Boxing"));
        courseAdapter = new CourseAdapter(getActivity(),sample);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        int width = getResources().getDisplayMetrics().widthPixels/2;
        int height = getResources().getDisplayMetrics().heightPixels/2;





        filter = (TextView)main.findViewById(R.id.filters_bt);



        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //blur.setVisibility(View.VISIBLE);
                drawerLayout.openDrawer(GravityCompat.END);


            }
        });
        setFilters();



        recyclerView.setAdapter(courseAdapter);


        getCourses(new GetCourses() {
            @Override
            public void Response(ArrayList<Courses> list) {
                sample = list;
                complete.addAll(sample);
                courseAdapter.notifyDataSetChanged();
            }
        });



        return main;
    }



    private void setFilters()
    {
        String[] planets = new String[] { "All", "Boxing, Core Fitness", "Boxing", "Abs & Butt, Core Fitness",
                "Kickboxing, Core Fitness", "Kickboxing", "Yoga"};
       fils= new ArrayList<String>();
        fils.addAll(Arrays.asList(planets));
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item_row, fils);  ;
        filterList.setAdapter(adapter);

        filterList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = (String) adapterView.getItemAtPosition(i);
                ArrayList<Courses> filtered =new ArrayList<Courses>() ;
                switch (selected)
                {
                    case "All":
                            courseAdapter.UpdateList(complete);
                        break;
                    case "Boxing, Core Fitness":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Boxing, Core Fitness"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);

                        break;
                    case "Boxing":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Boxing"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);
                        break;

                    case "Abs & Butt, Core Fitness":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Abs & Butt, Core Fitness"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);
                        break;

                    case "Kickboxing, Core Fitness":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Kickboxing, Core Fitness"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);
                        break;
                    case  "Kickboxing":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Kickboxing"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);
                        break;
                    case "Yoga":
                        filtered.clear();
                        for(Courses courses:complete )
                        {
                            if(courses.getSub_cat().toString().equals("Yoga"))
                            {
                                filtered.add(courses);
                            }
                        }
                        courseAdapter.UpdateList(filtered);
                        break;

                }
                drawerLayout.closeDrawers();

            }
        });

    }




    public void getCourses(final GetCourses callback)
    {

        AndroidNetworking.post("" +
                "http://sobekick.com/APP_FOLDER/sobekick/fetchVideos.php")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //try{
                        JSONObject object ;
                        if(response!=null)
                        {
                            for (int i=0;i<response.length();i++)
                            {

                                try {
                                    object = (JSONObject)response.get(i);
                                    sample.add(new Courses(object.getString("vidname"),object.getString("video_type"),object.getString("vidurl")));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        callback.Response(sample);


                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });


    }






    @Override
    public void onClick(View view) {


    }





}
