package com.example.leo.sobekick;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.leo.sobekick.Adapters.HomeViewPagerAdapter;
import com.example.leo.sobekick.Fragments.ProfileFragment;
import com.example.leo.sobekick.Helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class HomeActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private SessionManager sessionMan;
    private HomeViewPagerAdapter mHomeViewPagerAdapter;


    private int[] icons = {
            R.drawable.play_inactive,
            R.drawable.schedule_inactive,
            R.drawable.videos_inactive,
            R.drawable.profile_inactive



    };







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mTabLayout = (TabLayout)findViewById(R.id.sliding_tabs);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        sessionMan = new SessionManager(getApplicationContext());
        mHomeViewPagerAdapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mHomeViewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);



       /* try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.leo.sobekick",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

        /*
            new SessionManager.Builder()
                    .setContext(getApplicationContext())
                    .setPrefsName("login")
                    .build();
            new SessionManager.Builder()
                    .setContext(getApplicationContext())
                    .setPrefsName("customertoken")
                    .build();
        }*/


        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            mTabLayout.getTabAt(i).setIcon(icons[i]);
        }


        if(getIntent().getBooleanExtra("onDemand",false)) {
            TabLayout.Tab tab = mTabLayout.getTabAt(0);
            tab.select();
            tab.setIcon(R.drawable.play_active);
        }else {
            TabLayout.Tab tab = mTabLayout.getTabAt(3);
            tab.select();
            tab.setIcon(R.drawable.profile_active);
        }

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition())
                {
                    case 0:
                        tab.setIcon(R.drawable.play_active);
                        break;
                    case 1:
                        tab.setIcon(R.drawable.schedule_active);
                        break;
                    case 2:
                        tab.setIcon(R.drawable.videos_active);
                        break;
                    case 3:
                        tab.setIcon(R.drawable.profile_active);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition())
                {
                    case 0:
                        tab.setIcon(R.drawable.play_inactive);
                        break;
                    case 1:
                        tab.setIcon(R.drawable.schedule_inactive);
                        break;
                    case 2:
                        tab.setIcon(R.drawable.videos_inactive);
                        break;
                    case 3:
                        tab.setIcon(R.drawable.profile_inactive);
                        break;

                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




        Window window = this.getWindow();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS );
            window.setStatusBarColor(Color.TRANSPARENT);
        }



        if(sessionMan.isLoggedIn())
        {Checker.CheckAccess(getApplicationContext());}


    }
























    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ProfileFragment fragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + 3);;
        fragment.onActivityResult(requestCode, resultCode, data);
    }




    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()==0)
        {
            finish();

        }
        else {
            getSupportFragmentManager().popBackStack();
        }


        super.onBackPressed();
    }
}
