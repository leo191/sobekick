package com.example.leo.sobekick.Adapters;

import android.app.Activity;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leo.sobekick.Courses;
import com.example.leo.sobekick.Fragments.CourseVideo;
import com.example.leo.sobekick.Helper.SessionManager;
import com.example.leo.sobekick.HomeActivity;
import com.example.leo.sobekick.R;
import com.example.leo.sobekick.VideoActivity;

import java.util.ArrayList;

/**
 * Created by leo on 10/9/17.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyViewHolder> {
    private Context mContext;
    private SessionManager sessionManager;
    private ArrayList<Courses> c_list;

    View.OnClickListener clickListener;

    public void setOnItemClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView course_name,subcat;
        public Button takecourse;
        public CardView cardView;
        public MyViewHolder(View view) {
            super(view);
            takecourse = (Button)view.findViewById(R.id.take_course_btn);
            cardView = (CardView)view.findViewById(R.id.card_view);
            course_name = (TextView) view.findViewById(R.id.catagory_name);
            subcat = (TextView) view.findViewById(R.id.sub_catagory);
            Typeface custom_font = Typeface.createFromAsset(mContext.getAssets(),  "fonts/OpenSans-Regular.ttf");
            Typeface bold = Typeface.createFromAsset(mContext.getAssets(),  "fonts/OpenSans-Bold.ttf");
            Typeface italic = Typeface.createFromAsset(mContext.getAssets(),  "fonts/OpenSans-Italic.ttf");

            takecourse.setTypeface(bold);
            course_name.setTypeface(custom_font);
            subcat.setTypeface(italic);
        }
    }

    public CourseAdapter(Context context,ArrayList<Courses> c_list) {
        this.mContext = context;
        this.c_list = c_list;
        sessionManager = new SessionManager(mContext);
    }


    public void UpdateList(ArrayList<Courses> list)
    {

        this.c_list.clear();
        this.c_list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videos, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Courses courses = c_list.get(position);
        holder.course_name.setText(courses.getC_name());
        holder.subcat.setText(courses.getSub_cat());
        holder.takecourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(sessionManager.isLoggedIn()) {
                   if (SessionManager.both || SessionManager.onDemand) {
                       Intent intent = new Intent((AppCompatActivity) mContext, VideoActivity.class);
                       intent.putExtra("url", c_list.get(position).getIframe_url());
                       mContext.startActivity(intent);
                   } else {
                       AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
                       builder.setTitle("Subscribe");
                       builder.setMessage("Please Subscribe to Watch The Videos");
                       builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {

                           }
                       });
                       builder.show();
                   }
               }
               else {
                   AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
                   builder.setTitle("Login");
                   builder.setMessage("Please Login To watch The Videos");
                   builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {

                       }
                   });
                   builder.show();
               }
            }
        });

    }

    @Override
    public int getItemCount() {
        return c_list.size();
    }
}
