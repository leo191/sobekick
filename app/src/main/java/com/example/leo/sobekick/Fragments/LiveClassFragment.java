package com.example.leo.sobekick.Fragments;


import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leo.sobekick.Helper.SessionManager;
import com.example.leo.sobekick.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveClassFragment extends Fragment {
    WebView iframsView;
SessionManager sessionManager;
    public LiveClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         View liveView = inflater.inflate(R.layout.fragment_live_class, container, false);
         final TextView logtext = (TextView)liveView.findViewById(R.id.plsLogin);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/OpenSans-Regular.ttf");
        logtext.setTypeface(custom_font);
        logtext.setVisibility(View.GONE);
         sessionManager = new SessionManager(getActivity());
        if(sessionManager.isLoggedIn()) {

            if(SessionManager.both) {
                DisplayMetrics dm = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);


                WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getRealSize(size);
                int h = size.x;
                int w = size.y;


                int width = size.x;//dm.widthPixels;
                int height = size.y;//dm.heightPixels;
                String iframs = String.format("<!DOCTYPE HTML> <html><head></head> <body style=\"margin:0 0 0 0; padding:0 0 0 0;\"><iframe id=\"ls_embed_1495817289\" " +
                        "src=\"https://livestream.com/accounts/25137717/events/7402002/player?width=\"%d\"&" +
                        "height=\"%d\"&enableInfoAndActivity=false&defaultDrawer=&autoPlay=true&mute=false\"" +
                        " width=\"%d\" height=\"%d\" frameborder=\"0\" scrolling=\"no\" allowfullscreen> </iframe>" +
                        "<script type=\"text/javascript\" data-embed_id=\"ls_embed_1495817289\" " +
                        "src=\"https://livestream.com/assets/plugins/referrer_tracking.js\"></script></body> </html> ", width, height, width, height);


                // Inflate the layout for this fragment
                //String html = String.format("<iframe width=\"560\" height=\"%d\" src=\"https://www.youtube.com/embed/owsfdh4gxyc\" frameborder=\"0\" allowfullscreen></iframe>",200);
                iframsView = (WebView) liveView.findViewById(R.id.webiframe);

                iframsView.getSettings().setLoadWithOverviewMode(true);
                iframsView.getSettings().setUseWideViewPort(true);
                iframsView.setWebChromeClient(new WebChromeClient());
                iframsView.setWebViewClient(new WebViewClient());
                iframsView.setVerticalScrollBarEnabled(false);

                iframsView.setHorizontalScrollBarEnabled(false);
                iframsView.getSettings().setJavaScriptEnabled(true);
                iframsView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return (event.getAction() == MotionEvent.ACTION_MOVE);
                    }
                });
                iframsView.loadDataWithBaseURL("http://www.sobekick.com", iframs, "text/html", "UTF-8", null);
                //iframsView.loadData(html,"text/html",null);
            }
            else
            {
                logtext.setText("Please subscribe to watch videos");
                logtext.setVisibility(View.VISIBLE);
            }
       }
       else{

            logtext.setText("Please login to watch videos");
           logtext.setVisibility(View.VISIBLE);
       }

        return liveView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
