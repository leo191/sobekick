package com.example.leo.sobekick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.leo.sobekick.Helper.SessionManager;

public class SplaceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        if(sessionManager.isLoggedIn())
        {

            finish();
            startActivity(new Intent(SplaceActivity.this,HomeActivity.class).putExtra("onDemand",true));
        }
        else {

            startActivity(new Intent(SplaceActivity.this, GetStartedActivity.class));
        }
        finish();


    }
}
