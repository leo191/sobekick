package com.example.leo.sobekick.Callbacks;

import com.example.leo.sobekick.Courses;

import java.util.ArrayList;

/**
 * Created by leo on 10/9/17.
 */

public interface GetCourses {

    public abstract  void Response(ArrayList<Courses> list);

}
