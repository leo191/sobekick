package com.example.leo.sobekick.Helper;

/**
 * Created by leo on 17/9/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    public static boolean both, onDemand;
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences token;

    Editor editor,tokedit;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "sobekick";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String TOKEN = "token";


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);


        editor = pref.edit();
    }

    public void setToken(String token)
    {
        editor.putString(TOKEN,token);

    }
    public String getToken()
    {
        return pref.getString(TOKEN,"");
    }
    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}