package com.example.leo.sobekick.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.leo.sobekick.Fragments.LiveClassFragment;
import com.example.leo.sobekick.Fragments.OnDemandFragment;
import com.example.leo.sobekick.Fragments.ProfileFragment;
import com.example.leo.sobekick.Fragments.ScheduleFragment;
import com.example.leo.sobekick.R;

/**
 * Created by leo on 16/08/17.
 */

public class HomeViewPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;
    public HomeViewPagerAdapter(FragmentManager fm) {
        super(fm);
        //mContext =context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position)
        {
            case 0:
                fragment = new LiveClassFragment();

                break;
            case 1:
                fragment=new ScheduleFragment();
                break;
            case 2:
                fragment = new OnDemandFragment();
                break;
            case 3:
                fragment = new ProfileFragment();
                break;
        }

        return fragment;
    }


   /* @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.liveclass);
            case 1:
                return mContext.getString(R.string.schedule);
            case 2:
                return mContext.getString(R.string.ondemmand);
            case 3:
                return mContext.getString(R.string.profile);
            default:
                return null;
        }
    }*/

    @Override
    public int getCount() {
        return 4;
    }
}
