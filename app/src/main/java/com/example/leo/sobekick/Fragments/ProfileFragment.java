package com.example.leo.sobekick.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.leo.sobekick.Checker;
import com.example.leo.sobekick.Helper.SessionManager;
import com.example.leo.sobekick.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment  implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{


    LinearLayout loading;
    LoginButton fbLoginButt;
    SignInButton googleLoginButt;
    CallbackManager callbackManager;
    Button login_norm,logout_btn;
    GoogleApiClient googleApiClient;
    GoogleSignInOptions googleSignInOptions;
    private RelativeLayout relativeLayout;
    private ProgressBar progressBar;
    private ImageView fb_btn,google_btn;
    private static  int FB_CODE =100;
    private static int G_CODE = 200;
    private SessionManager sessionManager;
    final String sign_in_url = "https://api.cleeng.com/3.0/json-rpc";
    public static String token = "V8Q7pjRj05tcK4yNcizqtA8xoII_DcnMa44c2VjHz-YKR2Mf";
    View register_view;
    List<String> permissionNeeds= Arrays.asList("user_photos", "friends_photos", "email", "user_birthday", "user_friends");

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidNetworking.initialize(getContext());
        sessionManager = new SessionManager(getContext());
        callbackManager = CallbackManager.Factory.create();
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(getContext()).enableAutoManage(getActivity(),this).addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions).build();

    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


             register_view =  inflater.inflate(R.layout.register, container, false);
            //Toast.makeText(getActivity(),SessionManager.getString("login",""),Toast.LENGTH_SHORT).show();
            relativeLayout = (RelativeLayout)register_view.findViewById(R.id.relativeLayout);
            progressBar = (ProgressBar)register_view.findViewById(R.id.proglog);
//            loading = (LinearLayout)register_view.findViewById(R.id.progress);
//            loading.setVisibility(View.INVISIBLE);
            final TextView lets = (TextView)register_view.findViewById(R.id.letsgetstarted);
            final TextView brf = (TextView)register_view.findViewById(R.id.breif);
            final TextView forgot_btn = (TextView)register_view.findViewById(R.id.forgot_pass);
            final EditText email_username = (EditText)register_view.findViewById(R.id.username_edt);
            final EditText password_user = (EditText)register_view.findViewById(R.id.password_edt);


            fb_btn = (ImageView)register_view.findViewById(R.id.facebook_login);
            google_btn = (ImageView)register_view.findViewById(R.id.google_login);




             login_norm= (Button) register_view.findViewById(R.id.login_btn);
            logout_btn = (Button)register_view.findViewById(R.id.logout_button);


            Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/OpenSans-Regular.ttf");
            Typeface bold = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/OpenSans-Bold.ttf");


            lets.setTypeface(bold);
            brf.setTypeface(custom_font);
            brf.setClickable(true);
            brf.setMovementMethod(LinkMovementMethod.getInstance());
            login_norm.setTypeface(bold);
            logout_btn.setTypeface(custom_font);

            //Toast.makeText(getActivity(),SessionManager.getString("login",""),Toast.LENGTH_SHORT).show();

        if(sessionManager.isLoggedIn())
        {
            relativeLayout.setVisibility(View.VISIBLE);
            login_norm.setVisibility(View.INVISIBLE);

        }





        logout_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                 //   loggingOut();
                    sessionManager.setLogin(false);
                    relativeLayout.setVisibility(View.GONE);
                    login_norm.setVisibility(View.VISIBLE);
                }
            });

            login_norm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!email_username.getText().toString().isEmpty() || !password_user.getText().toString().isEmpty()){
                        progressBar.setVisibility(View.VISIBLE);
                        NormalLogin(email_username.getText().toString().trim(),password_user.getText().toString().trim(),token, sign_in_url);
                    }
                }
            });
            google_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    googleSignIn();
                }
            });

            LB();
           fb_btn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("email"));
               }
           });




            forgot_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FragmentManager fm = getFragmentManager();
                    ForgotFrag dialogFragment = new ForgotFrag ();
                    dialogFragment.show(fm, "Sample Fragment");




                }
            });


            return register_view;








    }








    private void LB()
    {

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {


                        loginWithFb(user.optString("email"),user.optString("name"),user.optString("id"));
                    }
                }).executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }





    private void googleSignIn()
    {

        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,G_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == G_CODE)
        {
            GoogleSignInResult googleResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(googleResult);
        }
        /*if(requestCode == FB_CODE)
        {
          */
        callbackManager.onActivityResult(requestCode, resultCode, data);
            /*if()) {
                return;
            }*/

        //}

    }




    private void handleResult(GoogleSignInResult result)
    {

        if(result.isSuccess())
        {
            GoogleSignInAccount googleAccount = result.getSignInAccount();
            loginWithGoogle(result.getSignInAccount().getEmail());
        }


    }










    private void loginWithGoogle(String email)
    {
        JSONObject main = new JSONObject();
        JSONObject par = new JSONObject();

        JSONArray jsonArray = new JSONArray();
        try {
            par.put("publisherToken",token);
            par.put("customerEmail",email);
            main.put("method","generateCustomerToken");
            main.put("params",par);
            main.put("jsonrpc","2.0");
            main.put("id",1);
            jsonArray.put(main);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post("https://api.cleeng.com/3.0/json-rpc")
                .addJSONArrayBody(jsonArray)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            JSONObject object   = (JSONObject)response.get(0);
                            if(object.isNull("result"))
                            {
                                SuggestRegister();
                            }
                            else{

                                Checker.CheckAccess(getContext());
                                sessionManager.setLogin(true);
                                String h = object.getJSONObject("result").getString("token");
                                sessionManager.setToken(object.getJSONObject("result").getString("token"));
                                relativeLayout.setVisibility(View.VISIBLE);
                                login_norm.setVisibility(View.GONE);
                                progressBar.setVisibility(View.INVISIBLE);
                               //logginIn(object.getJSONObject("result").getString("token"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });







    }


    /*private void loggingOut()
    {
        SessionManager.putString("customertoken","");
        SessionManager.putString("lognin","nok");
        relativeLayout.setVisibility(View.GONE);
        login_norm.setVisibility(View.VISIBLE);
    }

    private void logginIn(String token)
    {
        SessionManager.putString("customertoken",token);
        SessionManager.putString("lognin","ok");
        relativeLayout.setVisibility(View.VISIBLE);
        login_norm.setVisibility(View.GONE);
    }*/






    private void loginWithFb(String email,String name,String id)
    {


        progressBar.setVisibility(View.VISIBLE);

        JSONObject main = new JSONObject();
        JSONObject par = new JSONObject();

        JSONArray jsonArray = new JSONArray();
        try {
            par.put("publisherToken",token);
            par.put("facebookId",id);
            main.put("method","generateCustomerTokenFromFacebook");
            main.put("params",par);
            main.put("jsonrpc","2.0");
            main.put("id",1);
            jsonArray.put(main);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post("https://api.cleeng.com/3.0/json-rpc")
                .addJSONArrayBody(jsonArray)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            JSONObject object   = (JSONObject)response.get(0);
                            if(object.isNull("result"))
                            {
                                SuggestRegister();
                            }
                            else
                            {
                                Checker.CheckAccess(getContext());
                                sessionManager.setLogin(true);
                                String h = object.getJSONObject("result").getString("token");
                                sessionManager.setToken(object.getJSONObject("result").getString("token"));
                                relativeLayout.setVisibility(View.VISIBLE);
                                login_norm.setVisibility(View.GONE);
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });






    }





    private void SuggestRegister(){

        final TextView message = new TextView(getActivity());
        message.setPadding(24,16,24,16);
        message.setClickable(true);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "Login to your account and join one of our classes anytime, from anywhere."+
                "Don\'t have an account? Sign-up for a FREE trial <a href=\"http://www.sobekick.com/free-trial\">HERE</a>";
        message.setText(Html.fromHtml(text));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setTitle("No Account Found");
        builder.setView(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();

    }


















    public void SendRequest(String url,JSONArray jsonArray)
    {
      //  loading.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(url)
                .addJSONArrayBody(jsonArray)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //try{
                        try {
                            JSONObject object = (JSONObject) response.get(0);
                            if(!object.isNull("result"))
                            {
                                Checker.CheckAccess(getContext());
                                sessionManager.setLogin(true);
                                String h = object.getJSONObject("result").getString("token");
                                sessionManager.setToken(object.getJSONObject("result").getString("token"));
                                relativeLayout.setVisibility(View.VISIBLE);
                                login_norm.setVisibility(View.GONE);
                                progressBar.setVisibility(View.INVISIBLE);
                                //logginIn(object.getJSONObject("result").getString("token"));
                            }
                            else {
                                Toast.makeText(getActivity(),"Wrong Credentials!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                            progressBar.setVisibility(View.INVISIBLE);


                            Toast.makeText(getContext(),"Hi",Toast.LENGTH_SHORT).show();
                       /* }
                        catch (JSONException je)
                        {

                        }*/
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });

    }


    public void NormalLogin(String email,String pass,String token,String url)
    {
        JSONObject main = new JSONObject();
        JSONObject par = new JSONObject();

        JSONArray jsonArray = new JSONArray();
        try {
            par.put("publisherToken",token);
            par.put("customerEmail",email);
            par.put("password",pass);
            main.put("method","generateCustomerTokenFromPassword");
            main.put("params",par);
            main.put("jsonrpc","2.0");
            main.put("id",1);
            jsonArray.put(main);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        SendRequest(url,jsonArray);

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {

    }
}
