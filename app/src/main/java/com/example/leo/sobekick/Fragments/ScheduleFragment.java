package com.example.leo.sobekick.Fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.leo.sobekick.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.w3c.dom.Document;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {

    WebView calanderSched;
    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_schedule, container, false);






        calanderSched = (WebView)view.findViewById(R.id.webView);
        calanderSched.getSettings().setJavaScriptEnabled(true);
        String html = "<iframe width=\"100\" height=\"100\" style=\"border: 1px solid #cccccc;\" src=\"http://techslides.com/demos/sample-videos/small.mp4\" ></iframe>";
        calanderSched.loadUrl("http://sobekick.com/abc",null);
        calanderSched.setHorizontalScrollBarEnabled(false);





        calanderSched.setWebViewClient(new WebViewClient(){


            @Override
            public void onPageFinished (WebView view, String url) {
                view.loadUrl("javascript:(function() { " +
                        "var head = document.getElementsByTagName('header')[0];"
                        + "head.parentNode.removeChild(head);"+
                        "})()");
            }
        });
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }
}
