package com.example.leo.sobekick;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.leo.sobekick.Helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by leo on 18/9/17.
 */

public class Checker {
    public static final ArrayList<String> bothtoks = new ArrayList<>();
    public static final ArrayList<String> onDemandtoks = new ArrayList<>();
    private  static void SetAccess()
    {
        onDemandtoks.add(0,"data-offer-ic=\"S980331221_US\"");
        onDemandtoks.add(1,"data-offer-ic=\"S715534454_US\"");
        onDemandtoks.add(2,"data-offer-ic=\"S184141363_US\"");
        onDemandtoks.add(3,"data-offer-ic=\"S428708525_US\"");
        onDemandtoks.add(4,"data-offer-ic=\"S485185238_US\"");

        bothtoks.add(0,"data-offer-ic=\"S236784104_US\"");
        bothtoks.add(1,"data-offer-ic=\"S921096989_US\"");
        bothtoks.add(2,"data-offer-ic=\"S243704307_US\"");
        bothtoks.add(3,"data-offer-ic=\"S608970630_US\"");
        bothtoks.add(4,"data-offer-ic=\"S153696134_US\"");
        bothtoks.add(5,"data-offer-ic=\"S395184170_US\"");
        bothtoks.add(6,"data-offer-ic=\"S928224883_US\"");




    }

    public Checker(Context context, ArrayList<String> list)
    {


    }

    public static  void CheckAccess(final Context context)
    {
        SetAccess();
        SessionManager.both = false;
        SessionManager.onDemand = false;

        final SessionManager sessionManager = new SessionManager(context);

        for (String st:bothtoks
                ) {
            JSONObject main = new JSONObject();
            JSONObject par = new JSONObject();

            JSONArray jsonArray = new JSONArray();
            try {
                par.put("customerToken",sessionManager.getToken());
                par.put("offerId",st);
                par.put("ipAddress","");
                main.put("method","getAccessStatus");
                main.put("params",par);
                main.put("jsonrpc","2.0");
                main.put("id",1);
                jsonArray.put(main);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post("https://api.cleeng.com/3.0/json-rpc")
                    .addJSONArrayBody(jsonArray)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                JSONObject object   = (JSONObject)response.get(0);
                                if(object.getJSONObject("result").getString("accessGranted").equals("true"))
                                {
                                     SessionManager.both =true;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }





                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });



        }


        if(!SessionManager.both)
        {
            for (String st:onDemandtoks
                    ) {
                JSONObject main = new JSONObject();
                JSONObject par = new JSONObject();

                JSONArray jsonArray = new JSONArray();
                try {
                    par.put("customerToken",sessionManager.getToken());
                    par.put("offerId",st);
                    par.put("ipAddress","");
                    main.put("method","getAccessStatus");
                    main.put("params",par);
                    main.put("jsonrpc","2.0");
                    main.put("id",1);
                    jsonArray.put(main);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AndroidNetworking.post("https://api.cleeng.com/3.0/json-rpc")
                        .addJSONArrayBody(jsonArray)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {

                                try {
                                    JSONObject object   = (JSONObject)response.get(0);
                                    if(object.getJSONObject("result").getString("accessGranted").equals("true"))
                                    {
                                        SessionManager.onDemand =true;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }





                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });



            }
        }


    }

}
