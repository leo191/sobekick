package com.example.leo.sobekick.Fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.leo.sobekick.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotFrag extends DialogFragment {




    JSONObject obj;
    ProgressBar progressBar;
    public ForgotFrag() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidNetworking.initialize(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_forgot, container, false);
        final EditText email = (EditText)view.findViewById(R.id.forgot_mail);
        Button reset = (Button)view.findViewById(R.id.reset_pass);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar2);

        progressBar.setVisibility(View.INVISIBLE);


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject object = new JSONObject();

                JSONArray jsonArray = new JSONArray();
                try {
                    object.put("method","requestPasswordReset");
                    object.put("params",new JSONObject().put("publisherToken",ProfileFragment.token).put("customerEmail",email.getText().toString().trim()));
                    object.put("jsonrpc","2.0");
                    object.put("id", 1);
                    jsonArray.put(object);
                    SendRequest(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }
        });



        return view;
    }


    public void SendRequest(JSONArray jsonArray)
    {
        progressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.post("https://api.cleeng.com/3.0/json-rpc")
                .addJSONArrayBody(jsonArray)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //try{
                        progressBar.setVisibility(View.INVISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
                        try {
                            obj = ((JSONObject) response.get(0));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                            //String j = obj.getJSONObject("result").getString("success");
                            if (!obj.isNull("result")) {

                                builder.setTitle("Password Reset Link Sent");
                                builder.setMessage("A reset link has been sent to your email");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dismiss();
                                    }
                                });
                                builder.show();





                            } else {


                                builder.setTitle("Invalid Email");
                                builder.setMessage("Please Provide a Valid email");

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dismiss();
                                    }
                                });
                                builder.show();

                            }

                    }




                    @Override
                    public void onError(ANError anError) {

                    }


                });

    }



}
