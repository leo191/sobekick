package com.example.leo.sobekick.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.leo.sobekick.R;

import java.util.ArrayList;

/**
 * Created by leo on 7/9/17.
 */

public class FilterAdapter extends BaseAdapter {

    int layoutResourceId;
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<String> mDataSource;


    public FilterAdapter(Context mContext, ArrayList<String> mDataSource) {
        this.mContext = mContext;
        this.mDataSource = mDataSource;
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int i) {
        return mDataSource.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);
        TextView option_tv = (TextView)listItem.findViewById(R.id.options);
        option_tv.setText(mDataSource.get(position));


        return listItem;

    }
}
