package com.example.leo.sobekick;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class VideoActivity extends AppCompatActivity {

    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.transtool);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        final WebView iframsView = (WebView)findViewById(R.id.course_view);
        iframsView.setWebChromeClient(new WebChromeClient());
        iframsView.setWebViewClient(new WebViewClient());
        /*iframsView.setVerticalScrollBarEnabled(false);
        iframsView.setHorizontalScrollBarEnabled(false);*/
        iframsView.getSettings().setJavaScriptEnabled(true);
        iframsView.getSettings().setLoadWithOverviewMode(true);
        iframsView.getSettings().setUseWideViewPort(true);
        iframsView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        Intent intent =getIntent();
        url = intent.getStringExtra("url");


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;




        int h = this.getWindow().getDecorView().getBottom();


        String wmod = "width="+"\\\""+width+"\\\"";
        String hmod = "height="+"\\\""+height +"\\\"";
        String swmod = "width="+100;
        String shmod = "height="+100;
        Log.d("URL",url);
        url = url.replaceAll("width=\\\"960\\\"",wmod);
        url = url.replaceAll("height=\\\"540\\\"",hmod);
        url = url.replaceAll("width=960",swmod);
        url = url.replaceAll("height=540",shmod);
        url = "<!DOCTYPE HTML> <html><head></head> <body style=\"margin:0 0 0 0; padding:0 0 0 0;\">"+url+"</body> </html>";
        //iframsView.loadUrl("www.google.com");
        iframsView.loadDataWithBaseURL("http://www.sobekick.com",url,"text/html", "UTF-8",null);



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
